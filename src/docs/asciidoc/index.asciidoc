ifndef::snippets[]
:snippets: ../build/generated-snippets
endif::[]
= Restdocs Test API
test-api-docs
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 2

== 1. TEST CONTROLLER
---
=== [GET] hello
==== Request
include::{snippets}/hello-controller/hello/http-request.adoc[]

==== Response
include::{snippets}/hello-controller/hello/http-response.adoc[]

---

=== [GET] hello/dto
==== Parameters
include::{snippets}/hello-controller/dto/request-parameters.adoc[]

==== Request
include::{snippets}/hello-controller/dto/http-request.adoc[]

==== Response
include::{snippets}/hello-controller/dto/http-response.adoc[]
